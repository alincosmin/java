package com.pa_lab;

public class Main {

    public static void main(String[] args) {
        double startTime = System.currentTimeMillis();
        double computedPi = 4;
        double computedPi_old = 0;
        int numberOfDecimals = Integer.parseInt(args[0]);
        double power = (float)Math.pow(10,numberOfDecimals);
        float x = 1;
        do
        {
            if (x%2==0)
                computedPi += 4/(2*x+1);
            else
                computedPi -= 4/(2*x+1);
            x++;
            if((long)(computedPi*power) == (long) (computedPi_old*power))
                break;
            computedPi_old = computedPi;
        } while(true);
        double endTime = System.currentTimeMillis();
        System.out.println(("Computed pi: " + computedPi).substring(0,15+numberOfDecimals));
        System.out.println("In: " + (endTime-startTime) + "ms");
    }
}
