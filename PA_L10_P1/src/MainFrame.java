import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.html.ObjectView;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.sql.*;
import java.util.ArrayList;

public class MainFrame extends JFrame implements ActionListener
{
    private JButton executeButton;
    private JButton reportButton;
    private JDesktopPane desktopPane;
    private JLabel queryLabel;
    private JTextArea queryInput;

    public MainFrame(String title) {
        super(title);

        desktopPane = new JDesktopPane();
        desktopPane.putClientProperty("JDesktopPane.dragMode", "outline");
        executeButton = new JButton("Execute");
        executeButton.addActionListener(this);

        JPanel topPanel = new JPanel(true);

        queryLabel = new JLabel("Query");
        queryInput = new JTextArea(3,50);
        queryInput.setLineWrap(true);

        topPanel.add(queryLabel);
        topPanel.add(queryInput);
        topPanel.add(executeButton);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add("North", topPanel);
        getContentPane().add("Center", desktopPane);

        setSize(800,400);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        Dimension dim = getToolkit().getScreenSize();
        setLocation(dim.width/2-getWidth()/2,
                dim.height/2-getHeight()/2);
        setVisible(true);
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        addWindowListener(l);

        ArrayList<ArrayList<String>> metaData = new ServiceDAO().metaData();
        MetaResults(metaData.get(0).toArray(),metaData.get(1).toArray(),metaData.get(2).toArray());
    }

    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == executeButton)
        {
            String query = queryInput.getText();
            ServiceDAO service = new ServiceDAO();
            service.con = service.getConnection();
            Statement stmt;

            try
            {
                stmt = service.con.createStatement();

                ResultSet resultSet = stmt.executeQuery(query);
                ResultSetMetaData metaResults = resultSet.getMetaData();

                resultFrame(metaResults, resultSet);

                resultSet.close();
                stmt.close();
                service.closeConnection();
            }
            catch (Exception ex)
            {
                 ex.printStackTrace();
            }
        }
    }

    private void MetaResults(Object[] tables, Object[] functions, Object[] procedures)
    {
        JInternalFrame metaFrame1 = new JInternalFrame("Meta");
        metaFrame1.setBounds(0,0,350,205);
        JInternalFrame metaFrame2 = new JInternalFrame("Meta");
        metaFrame2.setBounds(0,220,350,205);
        JInternalFrame metaFrame3 = new JInternalFrame("Meta");
        metaFrame3.setBounds(0,440,350,205);

        JTable tablesTable = new JTable(new DefaultTableModel());
        tablesTable.setSize(340,200);
        JTable functionsTable = new JTable(new DefaultTableModel());
        functionsTable.setSize(340,200);
        JTable proceduresTable = new JTable(new DefaultTableModel());
        proceduresTable.setSize(340,200);

        DefaultTableModel model = (DefaultTableModel) tablesTable.getModel();

        model.addColumn("Tables");
        for(Object table : tables)
        {
            model.addRow(new Object[]{table});
        }

        model = (DefaultTableModel) functionsTable.getModel();
        model.addColumn("Functions");
        for(Object function : functions)
        {
            model.addRow(new Object[]{function});
        }

        model = (DefaultTableModel) proceduresTable.getModel();
        model.addColumn("Procedures");
        for(Object procedure : procedures)
        {
            model.addRow(new Object[]{procedure});
        }

        desktopPane.add(metaFrame1);
        desktopPane.add(metaFrame2);
        desktopPane.add(metaFrame3);

        metaFrame1.getContentPane().add(new JScrollPane(tablesTable));
        metaFrame2.getContentPane().add(new JScrollPane(functionsTable));
        metaFrame3.getContentPane().add(new JScrollPane(proceduresTable));
        metaFrame1.setVisible(true);
        metaFrame2.setVisible(true);
        metaFrame3.setVisible(true);
    }

    private void resultFrame(ResultSetMetaData metaSet, ResultSet set)
    {
        JButton reportButton = new JButton("Report");

        JInternalFrame resultFrame = new JInternalFrame("Results");
        resultFrame.setBounds(360,0,1000,650);

        JTable resultsTable = new JTable(new DefaultTableModel());
        DefaultTableModel tableModel = (DefaultTableModel) resultsTable.getModel();
        try
        {
            for (int i = 1; i <= metaSet.getColumnCount(); i++)
            {
                tableModel.addColumn(metaSet.getColumnLabel(i));
            }

            while(set.next())
            {
                ArrayList<Object> data = new ArrayList<Object>();
                for(int i=1; i<=metaSet.getColumnCount();i++)
                {
                    Object x = set.getObject(i);
                    if(set.wasNull())
                        data.add(null);
                    else data.add(x);
                }
                tableModel.addRow(data.toArray());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        desktopPane.add(resultFrame);
        JScrollPane scrollPane = new JScrollPane(resultsTable);
        scrollPane.setMaximumSize(new Dimension(1000,600));
        resultFrame.getContentPane().setLayout(new BorderLayout());
        resultFrame.getContentPane().add("Center", scrollPane);
        resultFrame.getContentPane().add("South",reportButton);

        resultFrame.setVisible(true);

        reportButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                new PdfReport(queryInput.getText());
            }
        });
    }
}
