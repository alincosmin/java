/**
 * Created by AlinCosmin on 5/26/2014.
 */

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Vector;

public class ServiceDAO
{
    public java.sql.Connection  con = null;
    private final String url = "jdbc:sqlserver://";
    private final String serverName= "localhost";
    private final String portNumber = "1434";
    private final String databaseName= "AdventureWorks2012";
    private final String userName = "sa";
    private final String password = "123asd";
    private final String selectMethod = "cursor";

    public ServiceDAO(){}

    public String getConnectionUrl(){
        return url+serverName+":"+portNumber+";databaseName="+databaseName+";selectMethod="+selectMethod+";";
    }

    public java.sql.Connection getConnection(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = java.sql.DriverManager.getConnection(getConnectionUrl(),userName,password);
            if(con!=null) System.out.println("Connection Successful!");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Error Trace in getConnection() : " + e.getMessage());
        }
        return con;
    }

    public ArrayList<ArrayList<String>> metaData()
    {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        con = getConnection();
        if(con!=null)
        {
            try
            {
                DatabaseMetaData metaData = con.getMetaData();
                ResultSet set = metaData.getTables(null,null,"%",null);
                ArrayList<String> tables = new ArrayList<String>();
                while(set.next())
                {
                    tables.add(set.getString(3));
                }
                result.add(tables);
                set.close();

                ArrayList<String> functions = new ArrayList<String>();
                set = metaData.getFunctions(null,null,"%");
                while(set.next())
                {
                    functions.add(set.getString(3));
                }
                result.add(functions);
                set.close();

                ArrayList<String> procedures = new ArrayList<String>();
                set = metaData.getProcedures(null,null,"%");
                while(set.next())
                {
                    procedures.add(set.getString(3));
                }
                result.add(procedures);
                set.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        closeConnection();
        return result;
    }

    public void closeConnection(){
        try{
            if(con!=null)
                con.close();
            con=null;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
