import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.JasperPdfExporterBuilder;
import net.sf.dynamicreports.report.builder.AbstractBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.definition.datatype.DRIDataType;
import net.sf.jasperreports.engine.JRDataSource;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import net.sf.jasperreports.engine.JasperReport;

import java.awt.*;
import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

public class PdfReport
{
    private ArrayList<String> _columns;
    public PdfReport(String query)
    {
        build(createDataSource(query));
    }

    private void build(JRDataSource dataSource)
    {
        try
        {
            JasperPdfExporterBuilder pdfExporter = export.pdfExporter("D:/Report.pdf");
            JasperReportBuilder reportBuilder = report();

            for(String column : _columns)
                reportBuilder.addColumn(col.column(column, column, type.stringType()));


            reportBuilder
                    .setTemplate(Templates.reportTemplate)
                    .pageFooter(cmp.pageXofY())
                    .setDataSource(dataSource)
                    .toPdf(pdfExporter);
            Desktop.getDesktop().open(new File("D:/Report.pdf"));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private JRDataSource createDataSource(String query)
    {
        ServiceDAO service = new ServiceDAO();
        if(service.con == null)
            service.con = service.getConnection();

        Statement stmt;
        DRDataSource dataSource = null;

        try
        {
            stmt = service.con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);
            ResultSetMetaData metaResults = resultSet.getMetaData();
            ArrayList<String> columns = new ArrayList<>();
            int columnCount = metaResults.getColumnCount();
            for(int i=1;i<=columnCount;i++)
                columns.add(metaResults.getColumnLabel(i));
            _columns = columns;
            dataSource = new DRDataSource(_columns.toArray(new String[_columns.size()]));

            while(resultSet.next())
            {
                ArrayList<String> rowData = new ArrayList<>();
                for(int i=1;i<=columnCount;i++)
                    rowData.add(resultSet.getString(i));
                dataSource.add(rowData.toArray());
            }
            stmt.close();
            resultSet.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if(service.con != null)
            service.closeConnection();

        return dataSource;
    }
}
