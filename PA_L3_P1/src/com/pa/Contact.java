package com.pa;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Contact
{
    public String Name = "";
    public long PhoneNumber = 0;
    public String Email = "";
    public Date Birthday;
    public SignType Sign;

    public String getName()
    {
        return Name;
    }

    public String getPhoneNumber()
    {
        return String.format("%l",PhoneNumber);
    }

    public String getEmail()
    {
        return Email;
    }

    public String getDate()
    {
        return Birthday.toString();
    }

    public String getSign()
    {
        return Sign.toString();
    }

    @Override
    public String toString()
    {
        return Name + " : " + PhoneNumber;
    }

    public enum SignType
    {
        Aries("Aries"),
        Taurus("Taurus") ,
        Gemini("Gemini"),
        Cancer("Cancer"),
        Leo("Leo"),
        Virgo("Virgo"),
        Libra("Libra"),
        Scorpio("Scorpio"),
        Sagittarius("Sagittarius"),
        Capricorn("Capricorn"),
        Aquarius("Aquarius"),
        Pisces("Pisces");

        private final String name;

        private SignType(String s)
        {
            name = s;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }
}
