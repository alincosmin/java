package com.pa;

import java.util.ArrayList;
import java.util.List;

public class Agenda
{
    private List<Contact> contacts = new ArrayList<Contact>();

    public List<Contact> ListContacts()
    {
        return contacts;
    }

    public void AddContact(Contact contact) throws AgendaException.InvalidPhoneNumber, AgendaException.InvalidNameFormat,
            AgendaException.NameExists, AgendaException.NumberExists
    {
        if(contact.PhoneNumber < 100)
            throw new AgendaException.InvalidPhoneNumber();
        if(!contact.Name.matches("\\w+( \\w+)|\\w*") || contact.Name.length() > 100)
            throw new AgendaException.InvalidNameFormat();
        for(Contact x : contacts)
        {
            if (x.Name == contact.Name)
                throw new AgendaException.NameExists();
            if (x.PhoneNumber == contact.PhoneNumber)
                throw new AgendaException.NumberExists();
        }
        contacts.add(contact);

    }

    public void RemoveContact(Contact target) throws AgendaException.ContactDoesntExist
    {
        if(!contacts.remove(target))
            throw new AgendaException.ContactDoesntExist();
    }

    public void RemoveContact(String targetName) throws AgendaException.NameDoesntExist
    {
        for(Contact con : contacts)
        {
            if(con.Name == targetName)
                if(contacts.remove(con))
                    return;
        }
        throw new AgendaException.NameDoesntExist();
    }

    public void RemoveContact(Long targetNumber) throws AgendaException.NumberDoesnExist
    {
        for(Contact con : contacts)
        {
            if(con.PhoneNumber == targetNumber)
                if(contacts.remove(con))
                    return;
        }
        throw new AgendaException.NumberDoesnExist();
    }
}
