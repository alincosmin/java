package com.pa;

public class AgendaException
{
    public static class InvalidPhoneNumber extends Exception {}
    public static class InvalidNameFormat extends Exception {}
    public static class NameExists extends Exception {}
    public static class NumberExists extends Exception {}
    public static class NumberDoesnExist extends Exception {}
    public static class NameDoesntExist extends Exception {}
    public static class ContactDoesntExist extends Exception {}
}

