package com.pa;

import freemarker.ext.beans.BeansWrapper;
import freemarker.template.*;
import com.google.gson.*;
import sun.misc.IOUtils;
import org.apache.commons.io.*;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception
    {
	    Agenda agenda = new Agenda();
        Contact contact1 = new Contact();
        contact1.Name = "Andrei";
        contact1.PhoneNumber = 341342;
        contact1.Sign = Contact.SignType.Gemini;
        Contact contact2 = new Contact();
        contact2.Name = "Mihaela";
        contact2.PhoneNumber = 333;
        contact2.Sign = Contact.SignType.Aquarius;
        Contact contact3 = new Contact();
        contact3.Name = "Gigi";
        contact3.PhoneNumber = 40747325505l;
        contact3.Sign = Contact.SignType.Gemini;
        agenda.AddContact(contact1);
        agenda.AddContact(contact2);
        agenda.AddContact(contact3);

        //agenda.RemoveContact("Mihai");

        BeansWrapper wrapper = new BeansWrapper();
        wrapper.setSimpleMapWrapper(true);
        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(wrapper);

        Template template = cfg.getTemplate("src/ZodiiContacte.ftl");
        Map<String, Object> root = new HashMap<String, Object>();
        Map<Contact.SignType, List<Contact>> contacts = ContactsBySign(agenda);
        root.put("contacts",contacts);
        Writer out = new OutputStreamWriter(System.out);
        template.process(root, out);
        out.flush();

        Agenda agenda2 = new Gson().fromJson(new FileReader("agenda.txt"),Agenda.class);
        System.out.println(agenda2.ListContacts());

        PrintWriter agendaFile = new PrintWriter("agenda.txt","UTF-8");
        agendaFile.print(new Gson().toJson(agenda));
        agendaFile.close();


    }

    public static Map<Contact.SignType,List<Contact>> ContactsBySign(Agenda agenda)
    {
        Map<Contact.SignType,List<Contact>> toReturn = new HashMap<Contact.SignType, List<Contact>>();
        for(Contact contact : agenda.ListContacts())
        {
            if(!toReturn.containsKey(contact.Sign))
                toReturn.put(contact.Sign,new ArrayList<Contact>());

            toReturn.get(contact.Sign).add(contact);
        }
        return toReturn;
    }
}
