package reteasociala;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bogdan Raducan <raducanb @ gmail.com>
 * @version 1.0 (current version number of program)
 * @since 2014-03-04 (the version of the package this class was first added to)
 */
public class ReteaSociala {
	private List<Person> _listaPersoane;
	private List<GrupPersoane> _listaGrupuri;
	
	public static void main(String[] args) {
		ReteaSociala Bookface = new ReteaSociala();
		Bookface.genereazaRetea(10, 4, 0.6, 0.7);
		
		for(GrupPersoane aGroup : Bookface.getListaGrupuri()) {
			System.out.println(aGroup.toString());
		}
	}
	
	public ReteaSociala() {
		_listaPersoane = new ArrayList<>();
		_listaGrupuri = new ArrayList<>();
	}
	
	/**
	 * Genereaza o retea sociala pe baza parametrilor dati ca argumente.
	 * 
	 * @param nrPersoane 	   numarul de persoane
	 * @param nrGrupuri 	   numarul de grupuri
	 * @param pbPersApartGrup  probabilitatea ca o persoana sa apartina unuia dintre grupuri
	 * @param pbPersRelDirecta probabilitatea ca oricare doua persoane sa aiba o legatura directa
	 */
	public void genereazaRetea(int nrPersoane, int nrGrupuri, double pbPersApartGrup, double pbPersRelDirecta) {
		List<Person> listaPersoane = new ArrayList<>();
		List<GrupPersoane> listaGrupuri = new ArrayList<>();
		
		for(int i=0; i< nrPersoane; i++) {
			listaPersoane.add(PersonFactory.createPerson("Pers" + i, "Last" + i, null));
		}
		
		for(int i=0; i<nrGrupuri; i++) {
			listaGrupuri.add(new GrupPersoane("grup" + i, null));
		}
		
		for(Person aPerson : listaPersoane) {
			for(GrupPersoane aGroup : listaGrupuri) {
				double randomChance = Math.random();
				if(randomChance < pbPersApartGrup) {
					aGroup.addPerson(aPerson.getID());
				}
			}
		}
		
		for(int i=0; i< listaPersoane.size() - 1; i++) {
			Person firstPerson = listaPersoane.get(i);
			for(int j=i+1; j < listaPersoane.size(); j++) {
				Person secondPerson = listaPersoane.get(j);
				double randomChange = Math.random();
				if(randomChange < pbPersRelDirecta) {
					firstPerson.addRelationWith(secondPerson.getID());
				}
			}
		}
		
		_listaPersoane = listaPersoane;
		_listaGrupuri = listaGrupuri;
	}

	public List<Person> getListaPersoane() {
		return _listaPersoane;
	}

	public void setListaPersoane(List<Person> arrayList) {
		this._listaPersoane = arrayList;
	}

	public List<GrupPersoane> getListaGrupuri() {
		return _listaGrupuri;
	}

	public void setListaGrupuri(List<GrupPersoane> _listaGrupuri) {
		this._listaGrupuri = _listaGrupuri;
	}

}
