package reteasociala;

@SuppressWarnings("serial")
public class PersonHasNoRelationsException extends Exception {
	public PersonHasNoRelationsException() { super(); }
	public PersonHasNoRelationsException(String message) { super(message); }
	public PersonHasNoRelationsException(String message, Throwable cause) { super(message, cause); }
	public PersonHasNoRelationsException(Throwable cause) { super(cause); }
}