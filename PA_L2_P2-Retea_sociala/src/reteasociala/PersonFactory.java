package reteasociala;

import java.util.List;
import java.util.ArrayList;

/**
 * @author      Bogdan Raducan 		<raducanb @ gmail.com>
 * @version     1.0                 
 * @since       2014-03-04          
 */
public class PersonFactory {
	static int _ID = 0;
	static List<Person> _peopleList = new ArrayList<>();
	
	/**
	 * Factory method for creating a person.
	 * It creates a person and adds it into the _peopleList List of
	 *  already created people.
	 *
	 * @param firstName  	 first name of the person
	 * @param lastName 		 last name for of the person
	 * @param relationWith   a list of Integers that the person has a direct relation with
	 *
	 * @return            created person
	 */
	public static Person createPerson(String firstName, String lastName, List<Integer> relationWith) {
		Person aPerson = new Person(firstName, lastName, _ID++);
		if(relationWith != null) {
			aPerson.setRelationWith(relationWith);
		}
		_peopleList.add(aPerson);
		return aPerson;
	}
	
	/**
	 * @throws PersonNotFoundException Throws this eception when the person with
	 * 									the specified ID is not found
	 */
	public static Person getPersonWithID(int ID) throws PersonNotFoundException {
		for(Person aPerson : _peopleList) {
			if(aPerson.getID() == ID) {
				return aPerson;
			}
		}
		throw new PersonNotFoundException("Persoana nu a fost gasita");
	}
	
	
}
