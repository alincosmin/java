package reteasociala;

@SuppressWarnings("serial")
public class PersonNotFoundException extends Exception {
	public PersonNotFoundException() { super(); }
	public PersonNotFoundException(String message) { super(message); }
	public PersonNotFoundException(String message, Throwable cause) { super(message, cause); }
	public PersonNotFoundException(Throwable cause) { super(cause); }
	
}