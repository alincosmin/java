package reteasociala;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bogdan Raducan <raducanb @ gmail.com>
 * @version 1.0 (current version number of program)
 * @since 2014-03-04 (the version of the package this class was first added to)
 */
public class Person {
	private String _firstName;
	private String _lastName;
	private int _ID;

	/**
	 * List containing ID's of people which are in relationship with this
	 * person.
	 */
	private List<Integer> _relationWith = new ArrayList<>();

	public Person(String firstName, String lastName, int ID) {
		_firstName = firstName;
		_lastName = lastName;
		_ID = ID;
	}

	/**
	 * Returns the people from the vector of ID's which are in a relation with
	 * this person.
	 * 
	 * @return if there are any relations, the vector of people which are in
	 *         relation with the person otherwise, it throws the
	 *         PersonHasNoRelations exception.
	 * 
	 */
	public List<Person> getDirectRelations()
			throws PersonHasNoRelationsException {
		if (_relationWith.size() == 0) {
			throw new PersonHasNoRelationsException(
					"Persoana nu are relatii cu nimeni");
		}

		List<Person> peopleInRelationWith = new ArrayList<>();

		for (Integer persID : _relationWith) {
			// Obs: initializez pentru a nu primi eroare. alta solutie?
			Person friend = new Person("", "", 0);
			boolean foundPerson = true;

			try {
				friend = PersonFactory.getPersonWithID(persID);
			} catch (PersonNotFoundException ex) {
				ex.printStackTrace();
				foundPerson = false;
			} finally {
				if (foundPerson) {
					peopleInRelationWith.add(friend);
				}
			}
		}

		return peopleInRelationWith;
	}

	/**
	 * Verifies if the person 'this' has a direct relation with the person given
	 * as argument.
	 * 
	 * Use {@link #addRelationWith(int ID)} to add a relation with a person.
	 * 
	 * @param persoanaDeVerificate
	 *            Persoana cu care se verifica legatura
	 * 
	 * @return Nivelul de legatura cu persoana
	 */
	public int verifyRelation(Person persoanaDeVerificat) {
		List<Person> alreadyVerifiedPeople = new ArrayList<>();
		int currentLevel = 1;
		boolean finished = false;

		for (Integer aPersonID : _relationWith) {
			try {
				alreadyVerifiedPeople.add(PersonFactory.getPersonWithID(aPersonID));
			} catch (PersonNotFoundException e) {
				e.printStackTrace();
			}
			if (aPersonID.equals(persoanaDeVerificat.getID())) {
				return currentLevel;
			}
		}

		List<Integer> searchingList = new ArrayList<>(_relationWith);
		Person searchedPerson = null;
		try {
			searchedPerson = PersonFactory.getPersonWithID(searchingList.get(0));
		} catch (PersonNotFoundException e) {
			e.printStackTrace();
		}
		
		while (!finished) {
			finished = true;
			currentLevel = 2;
			List<Person> directRelations = null;
			try {
				directRelations = searchedPerson.getDirectRelations();
			} catch (PersonHasNoRelationsException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < directRelations.size(); i++) {
				Person friendOfSearchedPerson = directRelations.get(i);
				if (!alreadyVerifiedPeople.contains(friendOfSearchedPerson)) {
					finished = false;
					alreadyVerifiedPeople.add(friendOfSearchedPerson);
					if(friendOfSearchedPerson.equals(persoanaDeVerificat)) {
						return currentLevel;
					}
				}
			}
		}

		return -1;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		this._firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		this._lastName = lastName;
	}

	public List<Integer> getRelationWith() {
		return _relationWith;
	}

	public void setRelationWith(List<Integer> _relationWith) {
		this._relationWith = _relationWith;
	}

	public void addRelationWith(int ID) {
		_relationWith.add(new Integer(ID));
	}

	public boolean removeRelationWith(int ID) {
		int friendToRemove = -1;
		Integer IDToCompare = new Integer(ID);

		if (_relationWith.contains(IDToCompare)) {
			_relationWith.remove(friendToRemove);
			return true;
		}
		return false;
	}

	public int getID() {
		return _ID;
	}
}
