package reteasociala;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author      Bogdan Raducan 		<raducanb @ gmail.com>
 * @version     1.0                 (current version number of program)
 * @since       2014-03-04          (the version of the package this class was first added to)
 */

public class GrupPersoane {
	private String _numeGrup;
	private List<Integer> _groupPeople = new ArrayList<>();

	public GrupPersoane(String numeGrup, List<Integer> groupPeople) {
		_numeGrup = numeGrup;
		if(groupPeople != null) {
			_groupPeople = groupPeople;
		}
	}

	public List<Integer> getGroupPeople() {
		return _groupPeople;
	}

	public void addPerson(int ID) {
		_groupPeople.add(new Integer(ID));
	}

	/**
	 * Override of the method that checks if two groups are equal.
	 *
	 * It uses {@link #equalLists(List<Integer> one, List<Integer> two)} to verify the equality of two lists.
	 *
	 * @return            true if the move is valid, otherwise false
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		final GrupPersoane other = (GrupPersoane) obj;
		if ((_groupPeople == null) ? (other.getGroupPeople() != null)
				: !equalLists(_groupPeople, other._groupPeople)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String groupString = new String();
		groupString += "Nume grup: " + _numeGrup + "\n";
		groupString += "Persoane grup:\n";
		
		for(Integer persID : _groupPeople) {
			Person aPers = null;
			try {
				aPers = PersonFactory.getPersonWithID(persID);
			} catch (PersonNotFoundException e) {
				e.printStackTrace();
			}
			
			groupString += "\t" + aPers.getFirstName() + " " + aPers.getLastName() + "\n";
		}
		groupString += "\n";
		
		return groupString;
	}
	
	public boolean equalLists(List<Integer> one, List<Integer> two) {
		if (one == null && two == null) {
			return true;
		}

		if ((one == null && two != null) || one != null && two == null
				|| one.size() != two.size()) {
			return false;
		}

		one = new ArrayList<Integer>(one);
		two = new ArrayList<Integer>(two);

		Collections.sort(one);
		Collections.sort(two);

		return one.equals(two);
	}

}
