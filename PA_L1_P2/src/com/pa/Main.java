package com.pa;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int nrOfPlayers;
        int lengthOfSequence;
        System.out.print("Nr. of players: ");
        nrOfPlayers = input.nextInt();
        System.out.print("Sequence length: ");
        lengthOfSequence = input.nextInt();
        Player[] players = new Player[nrOfPlayers];

        main_loop:{
            do
            {
                for(int i=0;i<nrOfPlayers;i++){
                    if(players[i] == null)
                        players[i] = new Player();
                    int index = (int)(Math.random()*26);
                    players[i].GiveLetter(index);
                    System.out.println("Player" + i + " has: " + players[i].CurrentSet());
                    String result = players[i].GiveValidSequence(lengthOfSequence);
                    if(result.length()>1){
                        System.out.println("Player" + i + " won with sequence: " + result);
                        break main_loop;
                    }
                }
            } while (true);
        }
    }
}
