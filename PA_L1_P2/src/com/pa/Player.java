package com.pa;

public class Player {
    public boolean letters[] = new boolean[26];
    public void GiveLetter(int index){
        letters[index] = true;
    }
    public String CurrentSet(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<26;i++)
            if(letters[i])
                sb.append((char)(i+65));
        return sb.toString();
    }
    public String GiveValidSequence(int length){
        for(int ratio=1; ratio<=26/length; ratio++){
            for(int secondIndex=0;secondIndex<26;secondIndex++){
                StringBuilder sb = new StringBuilder();
                for(int index=secondIndex; index<26; index+=ratio){
                    if(!letters[index])
                        break;
                    sb.append((char)(index+65));
                }
                if(sb.toString().length()>=length)
                    return sb.toString();
            }
        }
        return "";
    }
}
