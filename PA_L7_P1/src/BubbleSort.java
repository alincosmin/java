import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

/**
 * Created by AlinCosmin on 5/20/2014.
 */
public class BubbleSort
{
    private int[] target;
    private JLabel targetLabel;
    private long delay;
    private MyCanvas targetCanvas;

    BubbleSort(int[] array, JLabel label, MyCanvas canvas, long m)
    {
        target = array.clone();
        targetLabel = label;
        targetCanvas = canvas;
        delay = m;
    }

    public void sort()
    {
        int length = target.length;
        do
        {
            int x = 0;
            int i;
            for(i=1; i<length; i++)
            {
                if(target[i-1] > target[i])
                {
                    int aux = target[i-1];
                    target[i-1] = target[i];
                    target[i] = aux;
                    x = i;

                    try
                    {
                        targetCanvas.array = target.clone();
                        targetCanvas.repaint();
                        Thread.sleep(delay);
                    } catch (Exception e) {
                        System.out.println("Exception: "+e.toString());
                    }
                }
            }
            length = x;
        } while (length != 0);
        targetLabel.setText(targetLabel.getText() + " - Done");
    }
}
