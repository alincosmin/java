import java.awt.*;

/**
 * Created by AlinCosmin on 5/27/2014.
 */
public class MyCanvas extends Canvas
{
    public int[] array;

    public MyCanvas(int[] targetArray)
    {
        array = targetArray.clone();
        setSize(new Dimension(1000,150));
        setBackground(Color.WHITE);
    }

    public void paint(Graphics g)
    {
        g.setColor(Color.GREEN);
        for(int i=0; i<array.length; i++)
        {
            g.fillRect(i * 30, 150 - array[i], 20, array[i]);
        }
    }
}
