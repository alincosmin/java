import javax.swing.*;
import java.util.Arrays;

/**
 * Created by AlinCosmin on 5/20/2014.
 */
public class HeapSort
{
    private int[] target;
    private JLabel targetLabel;
    private long delay;
    private MyCanvas targetCanvas;

    private int N;

    HeapSort(int[] array, JLabel label, MyCanvas canvas,long m)
    {
        target = array.clone();
        targetLabel = label;
        targetCanvas = canvas;
        delay = m;
    }

    public void sort()
    {
        heapify();
        for(int i=N; i>0; i--)
        {
            swap(0,i);
            N = N-1;
            maxheap(0);
        }
        targetLabel.setText(targetLabel.getText() + " - Done");
    }

    private void swap(int i, int j)
    {
        int aux = target[i];
        target[i] = target[j];
        target[j] = aux;

        try
        {
            targetCanvas.array = target.clone();
            targetCanvas.repaint();
            Thread.sleep(delay);
        } catch (Exception e) {
            System.out.println("Exception: "+e.toString());
        }
    }

    private void heapify()
    {
        N = target.length-1;
        for(int i=N/2; i>=0; i--)
            maxheap(i);
    }

    private void maxheap(int i)
    {
        int left = 2*i;
        int right = 2*i+1;
        int max = i;

        if(left<=N && target[left] > target[i])
            max = left;
        if(right<=N && target[right] > target[max])
            max = right;
        if(max !=i)
        {
            swap(i, max);
            maxheap(max);
        }
    }
}
