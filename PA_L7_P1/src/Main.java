import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by AlinCosmin on 5/19/2014.
 */
public class Main
{
    public static void main(String[] args)
    {
        int[] targetArray = new int[34];
        for(int i=0; i<34; i++)
            targetArray[i] = Math.abs(new Random().nextInt()%149) + 1;
        new MainFrame("Concurent sorting", targetArray, 100);
    }
}
