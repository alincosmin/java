import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.*;

public class MainFrame extends JFrame
{
    private JLabel targetArrayLabel = new JLabel("Array: ");
    private JButton startButton = new JButton("Start");

    private JLabel bubbleSortLabel = new JLabel("BubbleSort: ");
    private JLabel quickSortLabel = new JLabel("QuickSort: ");
    private JLabel heapSortLabel = new JLabel("HeapSort: ");

    private MyCanvas bubbleSortCanvas;
    private MyCanvas quickSortCanvas;
    private MyCanvas heapSortCanvas;


    private final int[] targetArray;
    private long delay;

    public MainFrame(String title, int[] array, long pdelay)
    {
        super(title);

        delay = pdelay;
        targetArray = array;
        targetArrayLabel.setText(targetArrayLabel.getText() + Arrays.toString(targetArray));

        SpringLayout layout = new SpringLayout();
        setLayout(layout);
        GridBagConstraints gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.HORIZONTAL;

        targetArrayLabel.setFont(new Font("serif", Font.BOLD, 16));
        bubbleSortLabel.setFont(new Font("serif", Font.BOLD, 16));
        quickSortLabel.setFont(new Font("serif", Font.BOLD, 16));
        heapSortLabel.setFont(new Font("serif", Font.BOLD, 16));

        bubbleSortCanvas = new MyCanvas(targetArray);
        bubbleSortCanvas.setBackground(Color.WHITE);
        quickSortCanvas = new MyCanvas(targetArray);
        heapSortCanvas = new MyCanvas(targetArray);

        add(targetArrayLabel);
        add(bubbleSortLabel);
        add(bubbleSortCanvas);
        add(quickSortLabel);
        add(quickSortCanvas);
        add(heapSortLabel);
        add(heapSortCanvas);
        add(startButton);

        layout.putConstraint(SpringLayout.WEST, targetArrayLabel,25, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, targetArrayLabel,25, SpringLayout.NORTH, this);

        layout.putConstraint(SpringLayout.NORTH, bubbleSortLabel,180, SpringLayout.NORTH, targetArrayLabel);
        layout.putConstraint(SpringLayout.WEST, bubbleSortLabel,25, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, bubbleSortCanvas, -55, SpringLayout.NORTH, bubbleSortLabel);
        layout.putConstraint(SpringLayout.WEST, bubbleSortCanvas, 200, SpringLayout.WEST, bubbleSortLabel);

        layout.putConstraint(SpringLayout.NORTH, quickSortLabel,180, SpringLayout.NORTH, bubbleSortLabel);
        layout.putConstraint(SpringLayout.WEST, quickSortLabel,25, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, quickSortCanvas, -55, SpringLayout.NORTH, quickSortLabel);
        layout.putConstraint(SpringLayout.WEST, quickSortCanvas, 200, SpringLayout.WEST, quickSortLabel);

        layout.putConstraint(SpringLayout.NORTH, heapSortLabel,180, SpringLayout.NORTH, quickSortLabel);
        layout.putConstraint(SpringLayout.WEST, heapSortLabel,25, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, heapSortCanvas, -55, SpringLayout.NORTH, heapSortLabel);
        layout.putConstraint(SpringLayout.WEST, heapSortCanvas, 200, SpringLayout.WEST, heapSortLabel);

        layout.putConstraint(SpringLayout.NORTH, startButton, 670, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, startButton, 1270, SpringLayout.WEST, this);

        startButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                start();
            }
        });

        setSize(800, 400);
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

    }

    private void start()
    {
        SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>()
        {
            SwingWorker<Boolean, Void> worker1 = new SwingWorker<Boolean, Void>()
            {
                @Override
                protected Boolean doInBackground() throws Exception
                {
                    new BubbleSort(targetArray, bubbleSortLabel,bubbleSortCanvas, delay).sort();
                    return true;
                }
            };

            SwingWorker<Boolean, Void> worker2 = new SwingWorker<Boolean, Void>()
            {
                @Override
                protected Boolean doInBackground() throws Exception
                {
                    new QuickSort(targetArray, quickSortLabel,quickSortCanvas ,delay).sort();
                    return true;
                }
            };

            SwingWorker<Boolean, Void> worker3 = new SwingWorker<Boolean, Void>()
            {
                @Override
                protected Boolean doInBackground() throws Exception
                {
                    new HeapSort(targetArray, heapSortLabel,heapSortCanvas ,delay).sort();
                    return true;
                }
            };

            @Override
            protected Boolean doInBackground() throws Exception
            {
                worker1.execute();
                worker2.execute();
                worker3.execute();
                return true;
            }
        };

        worker.execute();
    }

    public static void changeSorterLabel(final JLabel targetLabel, final int[] array)
    {
        Thread worker = new Thread()
        {
            public void run()
            {
                final int[] finalArray = array;
                SwingUtilities.invokeLater(new Runnable() {
                    public void run()
                    {
                        targetLabel.setText(targetLabel.getText());
                    }
                });
            }
        };
        worker.start();
    }

}