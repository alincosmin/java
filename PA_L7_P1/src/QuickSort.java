import javax.swing.*;
import java.util.Arrays;

/**
 * Created by AlinCosmin on 5/20/2014.
 */
public class QuickSort
{
    private int[] target;
    private JLabel targetLabel;
    private MyCanvas targetCanvas;
    private long delay;

    QuickSort(int[] array, JLabel label, MyCanvas canvas, long m)
    {
        target = array.clone();
        targetLabel = label;
        targetCanvas = canvas;
        delay = m;
    }

    public void sort()
    {
        QuickSortCore(0,target.length-1);
        targetLabel.setText(targetLabel.getText() + " - Done");
    }

    private void QuickSortCore(int low, int high)
    {
        int i = low;
        int j = high;
        int pivot = target[low+(high-low)/2];

        while(i<=j)
        {
            while(target[i] < pivot) {i++;}
            while(target[j] > pivot) {j--;}
            if (i<=j)
            {
                int aux = target[i];
                target[i] = target[j];
                target[j] = aux;
                i++;
                j--;

                try
                {
                    targetCanvas.array = target.clone();
                    targetCanvas.repaint();
                    Thread.sleep(delay);
                } catch (Exception e) {
                    System.out.println("Exception: "+e.toString());
                }
            }
        }
        if(low < j) QuickSortCore(low,j);
        if(i < high) QuickSortCore(i,high);
    }
}
