package com.pa;

public class Main {

    public static void main(String[] args) {
        SparseMatrix matrix1 = new SparseMatrix();
        matrix1.Set(0,0,1);
        double[][] test = {{0.,2.},{0.,3.}};
        SparseMatrix matrix2 = new SparseMatrix(test);

        matrix1.MultiplyWith(matrix2);
        System.out.println(matrix1);
    }
}
