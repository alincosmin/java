package com.pa;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class SparseMatrix
{
    private List<MyTuple<Integer,Integer,Double>> elements = new ArrayList<MyTuple<Integer, Integer, Double>>();

    // Constructor generic
    SparseMatrix() {}

    // Generarea unui matrice binare dintr-un vector
    SparseMatrix(double[] array, int columns)
    {
        int index = 0;
        int column = 0;
        for (double z : array)
        {
            Set(index/columns,column,z);
            column = (column + 1) % columns;
            index++;
        }
    }

    // Generarea unei matrici rare dintr-o matrice rara
    SparseMatrix(double[][] matrix)
    {
        int x = 0;
        int y = 0;
        for (double[] row : matrix)
        {
            y = 0;
            for(double z : row)
            {
                Set(x,y,z);
                y++;
            }
            x++;
        }
    }

    // Returneaza valoarea de pe pozitia (x,y); in cazul in care ea nu exist, raspunsul este 0;
    public double Get(int x, int y)
    {
        for(MyTuple<Integer,Integer,Double> element : elements)
        {
            if(element.x == x && element.y == y)
                return element.z;
        }
        return 0;
    }

    // Setarea unei valori pe pozitia (x,y); pentru 0, valoarea stocata deja, se sterge.
    public int Set(int x, int y, double newValue)
    {
        int target = -1;
        for(MyTuple<Integer,Integer,Double> element : elements)
        {
            if(element.x == x && element.y == y)
            {
                if(newValue == 0)
                    target = elements.indexOf(element);
                else
                {
                    element.z = newValue;
                    return 0;
                }
                break;
            }
        }

        if(target != -1)
        {
            elements.remove(target);
            return -1;
        }

        if (newValue == 0)
            return 0;

        int index = -1;
        MyTuple<Integer,Integer,Double> newElement = new MyTuple(x,y,newValue);
        for(MyTuple<Integer,Integer,Double> element : elements)
        {
            if(newElement.x < element.x || (newElement.x == element.x && newElement.y < element.y))
            {
                index = elements.indexOf(element);
                break;
            }
        }
        if(index == -1)
            elements.add(newElement);
        else
            elements.add(index,newElement);
        return 1;


    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for(MyTuple<Integer,Integer,Double> element : elements)
        {
            sb.append(element+"\n");
        }
        return sb.toString();
    }

    // Insumarea matricei curnte cu calendar,
    public void AddWith(int scalar)
    {
        for(MyTuple<Integer,Integer,Double> element : elements)
            element.z += scalar;
    }

    // Insumarea matricei curente cu o alta matrice
    public void AddWith(SparseMatrix matrix)
    {
        for(MyTuple<Integer, Integer, Double> element : matrix.elements)
        {
            double z = this.Get(element.x,element.y);
            this.Set(element.x,element.y,z+element.z);
        }
    }

    // Inmultirea matricei curente cu un scalar
    public void MultiplyWith(int scalar)
    {
        for(MyTuple<Integer,Integer,Double> element : elements)
            element.z *= scalar;
    }

    // Transpusa matricei curente
    public SparseMatrix Transpose()
    {
        SparseMatrix transpose = new SparseMatrix();
        for(MyTuple<Integer,Integer,Double> element : this.elements)
        {
            transpose.Set(element.y,element.x,element.z);
        }
        return transpose;
    }

    // Inmultirea matricei curente cu o alta matrice
    public void MultiplyWith(SparseMatrix matrix)
    {
        SparseMatrix destination = new SparseMatrix();
        double z;
        for(MyTuple<Integer,Integer,Double> element : this.elements)
        {
            for(MyTuple<Integer,Integer,Double> element2 : matrix.Transpose().elements)
            {
                if(element.y == element2.y)
                {
                    z = destination.Get(element.x,element2.x);
                    destination.Set(element.x,element2.x,z+element.z*element2.z);
                }
            }
        }
        this.elements = destination.elements;
    }

    // Generare aleatorie a nrOfValues valori nenule, intr-o matrice de maxim 100x100, valorile fiind intre minValue si manValues
    public static SparseMatrix RandomMatrix(int nrOfValues, double minValue, double maxValue)
    {
        SparseMatrix matrix = new SparseMatrix();
        int values = 0;
        int row;
        int column;
        double value;
        while(values <= nrOfValues)
        {
            row = (int)(Math.random() * 100);
            column = (int)(Math.random() * 100);
            value = minValue + Math.random() * ((maxValue-minValue) + 1);
            values += matrix.Set(row,column,value);
        }
        return matrix;
    }

    // Verificare simpla a egalitatii a doua obiecte prin acest obeiect special.
    @Override
    public boolean equals(Object matrix)
    {
        return this.hashCode() == ((SparseMatrix)matrix).hashCode();
    }


    // Un simplu algoritmn pentru generarea unui hashcode;.
    // Nu garanteaza rezistenta mare la coliziuni!
    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 13 + elements.size();
        hash = (int) (hash * 3 + (elements.size() >= 1? elements.get(1).z : 0));
        hash = hash * 17 + (int) Get(1,7);
        hash = hash * 37 + (int) Get(3,7);
        hash = (int) (hash * 5 + (elements.size() >= 3? elements.get(3).z : 0));
        hash = (int) (hash * 7 + (elements.size() >= 5? elements.get(5).z : 0));
        hash = (int) (hash * 11 + (elements.size() >= 7? elements.get(7).z : 0));
        hash *= 57;

        return hash;
    }
}
