package com.pa;

// Tupla de 3 elemente, necesare construirii matricei rare
// x - randul, y - coloana, y - informatia ( valoarea )
public class MyTuple<X, Y, Z>
{
    public X x;
    public Y y;
    public Z z;
    public MyTuple(X x, Y y, Z z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString()
    {
        return String.format("(%d, %d, %f)",x,y,z);
    }
}
